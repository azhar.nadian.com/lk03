/*
* Jangan ubah kode didalam kelas Main ini
* Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
* Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?

* "Dalam konteks kalkulator, abstract class lebih cocok digunakan jika terdapat hubungan hierarki antara 
kelas-kelas kalkulator, implementasi default diperlukan untuk beberapa metode, atau jika ada kebutuhan untuk 
menyimpan keadaan bersama antara beberapa metode." 
*/

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
 
        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();
 
        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();
 
        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();
 
        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }
 
        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}

 